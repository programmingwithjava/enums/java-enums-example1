public enum Coordinates {

    LOC1(44.968046, -94.420307), LOC2(44.33328, -89.132008), LOC3(44.920474, -93.447851);

    private double lat;
    private double lon;

    private Coordinates(double lat, double lon){
        this.lat = lat;
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }
}
